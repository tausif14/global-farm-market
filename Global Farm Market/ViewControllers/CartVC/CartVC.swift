//
//  CartVC.swift
//  Global Farm Market
//
//  Created by M.Hashim on 04/11/2019.
//  Copyright © 2019 index. All rights reserved.
//

import UIKit

class CartVC: BaseVC {
    //MARK:- OutLets
    @IBOutlet weak var cartCollectionView: UICollectionView!
    @IBOutlet weak var toolView: UIView!//forBoth Tool and Labour
    @IBOutlet weak var checkView: UIView!
    @IBOutlet weak var menuControl: UISegmentedControl!
    @IBOutlet weak var labourView: UIView!
    
    //MARK:- Variables
    var cartArray = [cartStruct]()
    //MARK:- LIFECYLCE
    override func viewDidLoad() {
        super.viewDidLoad()
        cartArray = [cartStruct(imageVar: #imageLiteral(resourceName: "Potato"), buttonLabel: "Sell"),cartStruct(imageVar: #imageLiteral(resourceName: "Potato"), buttonLabel: "Free"),cartStruct(imageVar: #imageLiteral(resourceName: "Onion"), buttonLabel: "Exchange"),cartStruct(imageVar: #imageLiteral(resourceName: "Potato"), buttonLabel: "Sell"),cartStruct(imageVar: #imageLiteral(resourceName: "Potato"), buttonLabel: "Free"),cartStruct(imageVar: #imageLiteral(resourceName: "Onion"), buttonLabel: "Exchange"),cartStruct(imageVar: #imageLiteral(resourceName: "Potato"), buttonLabel: "Sell"),cartStruct(imageVar: #imageLiteral(resourceName: "Potato"), buttonLabel: "Free"),cartStruct(imageVar: #imageLiteral(resourceName: "Onion"), buttonLabel: "Exchange")]
        setNavLeftImage(image: #imageLiteral(resourceName: "small logo dummy"))
        setLayout()
        cartCollectionView.reloadData()
    }
    @IBAction func MenuChangeAction(_ sender: Any) {
        switch menuControl.selectedSegmentIndex{
        case 0:
            cartCollectionView.isHidden = false
            toolView.isHidden = true
            checkView.isHidden = true
            labourView.isHidden = true
        case 1:
            cartCollectionView.isHidden = true
            toolView.isHidden = false
            checkView.isHidden = false
            labourView.isHidden = true
        case 2:
            cartCollectionView.isHidden = true
            toolView.isHidden = false
            checkView.isHidden = true
            labourView.isHidden = false
        default:
            break
        }
    }
}
extension CartVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    //MARK:- collectionView_Delegates
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cartArray.count
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: cartCollectionView.frame.width/3.5, height: cartCollectionView.frame.width/3)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cartData = cartArray[indexPath.row]
        guard let cell = cartCollectionView.dequeueReusableCell(withReuseIdentifier: "CartCell", for: indexPath) as? CartCell else{
            return UICollectionViewCell()
        }
        cell.cartImg.image = cartData.imageVar
        cell.cartBtn.setTitle(cartData.buttonLabel, for: .normal)
        return cell
    }
    
    func setLayout(){
        circleShap(imageMsg: nil, actionBtn: nil, customView: toolView)
        let cellSize = CGSize(width: cartCollectionView.frame.width/3.5, height: cartCollectionView.frame.width/3)

//        let layout = UICollectionViewFlowLayout()
//        layout.scrollDirection = .vertical //.horizontal
//        layout.itemSize = cellSize
////        layout.sectionInset = UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1)
////        layout.minimumLineSpacing = 1.0
////        layout.minimumInteritemSpacing = 1.0
//        print("*********Cell value :\((width: cartCollectionView.frame.width/3.5, height: cartCollectionView.frame.width/3))************")
//        cartCollectionView.setCollectionViewLayout(layout, animated: true)
    }
    
    
}
struct cartStruct{
    var imageVar : UIImage?
    var buttonLabel : String?
}
