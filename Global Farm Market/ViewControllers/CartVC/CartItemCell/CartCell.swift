//
//  CartCell.swift
//  Global Farm Market
//
//  Created by M.Hashim on 05/11/2019.
//  Copyright © 2019 index. All rights reserved.
//

import UIKit

class CartCell: UICollectionViewCell {
    //MARK:- Outlets

    @IBOutlet weak var cartImg: UIImageView!
    @IBOutlet weak var cartBtn: UIButton!
    
    
    
    //MARK:- LIFECYLCE
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       circleShap(imageMsg: cartImg, actionBtn: cartBtn)
    }

}
extension UICollectionViewCell{
    func circleShap(imageMsg:UIImageView?=nil,actionBtn:UIButton){
         imageMsg?.layer.cornerRadius = AppDelegate.deviceRatio/14
         actionBtn.layer.cornerRadius = AppDelegate.deviceRatio/14
         print("*********DeviceRatio Value for Cell :\(AppDelegate.deviceRatio/12)*********")
    }
}
