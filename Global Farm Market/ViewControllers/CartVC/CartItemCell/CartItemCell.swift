//
//  CartItemCell.swift
//  Global Farm Market
//
//  Created by M.Hashim on 05/11/2019.
//  Copyright © 2019 index. All rights reserved.
//

import UIKit

class CartItemCell: UITableViewCell {
    //MARK:- Outlets
    @IBOutlet weak var fistLbl: UILabel!
    @IBOutlet weak var secondLbl: UILabel!
    
    @IBOutlet weak var msgImg: UIImageView!
    @IBOutlet weak var msgLbl: UILabel!
    
    //MARK:- LIFECYLCE
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //circleShap(imageMsg: msgImage)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
   
}
