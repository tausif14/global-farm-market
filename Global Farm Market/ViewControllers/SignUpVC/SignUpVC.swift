//
//  SignUpVC.swift
//  Global Farm Market
//
//  Created by M.Hashim on 01/11/2019.
//  Copyright © 2019 index. All rights reserved.
//

import UIKit

class SignUpVC: BaseVC {
    
    //MARK:- OUTLETS
    @IBOutlet weak var signUpBtn: UIButton!
    @IBOutlet weak var firstNameTxt: UITextField!
    @IBOutlet weak var lastNameTxt: UITextField!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!
    @IBOutlet weak var confrimTxt: UITextField!
    @IBOutlet weak var loginLbl: UILabel!
    
    
    //MARK:- VARIABLES
    
    
    //MARK:- LIFECYLCE
    override func viewDidLoad() {
        super.viewDidLoad()
        setLayOut()
        gotoSignUp()
    }
    override func viewWillAppear(_ animated: Bool) {
        refreshFields()
    }

    @objc func tapFunction(sender:UITapGestureRecognizer) {
       goBack()
       }
    func refreshFields(tag:Int?=0){
//        if tag == 1{
//            txt.text = UITextField.setTextClear(txt: UITextField)
//        }
        firstNameTxt.becomeFirstResponder()
        firstNameTxt.text = ""
        lastNameTxt.text = ""
        emailTxt.text = ""
        passwordTxt.text = ""
        confrimTxt.text = ""
    }
    //MARK:- navigationFucntion
       func gotoSignUp(){
           
           let tap = UITapGestureRecognizer(target: self, action: #selector(tapFunction(sender:)))
           loginLbl.isUserInteractionEnabled = true
           loginLbl.addGestureRecognizer(tap)
       }
    //MARK:- cutomizeFields
       func setLayOut(){
           signUpBtn.layer.cornerRadius = 25
           firstNameTxt.layer.cornerRadius = firstNameTxt.frame.height/2
           lastNameTxt.layer.cornerRadius = lastNameTxt.frame.height/2
           emailTxt.layer.cornerRadius = emailTxt.frame.height/2
           passwordTxt.layer.cornerRadius = passwordTxt.frame.height/2
           confrimTxt.layer.cornerRadius = confrimTxt.frame.height/2
           self.navigationController?.navigationBar.isHidden=true

       }
    @IBAction func signUpAction(_ sender: Any) {
        if !(firstNameTxt.text?.isEmpty ?? false) || !(lastNameTxt.text?.isEmpty ?? false) || !(emailTxt.text?.isEmpty ?? false) || !(passwordTxt.text?.isEmpty ?? false) || !(confrimTxt.text?.isEmpty ?? false){
            if validateEmailPassword(emailText: emailTxt.text ?? "", password: passwordTxt.text ?? "", confrimPassward: confrimTxt.text ?? "") == true{
                //Login
            }
            else{
            makeAlert(messageData: "Field are Invaild")
            }
        }
        else{
             makeAlert(messageData: "Please Enter All Fields")
        }
    }
    
}

