//
//  SettingVC.swift
//  Global Farm Market
//
//  Created by M.Hashim on 04/11/2019.
//  Copyright © 2019 index. All rights reserved.
//

import UIKit

class SettingVC: BaseVC {
    //MARK:- Outlets
    @IBOutlet weak var msgTableView: UITableView!
    //MARK:- Variables
    
    var settingArray = [settingStruct]()
    
    //MARK:- LIFECYLCE
    override func viewDidLoad() {
        super.viewDidLoad()
        settingArray = [settingStruct(imageVar:#imageLiteral(resourceName: "invite"), firstLabel: "Invite Friends", secondLabel: "Follow", thirdLabel: "Profile")
            ,settingStruct(imageVar: #imageLiteral(resourceName: "push"), firstLabel: "Push", secondLabel: "Likes", thirdLabel: "Direct"),settingStruct(imageVar: #imageLiteral(resourceName: "comments,account"), firstLabel: "Comments", secondLabel: "Account Privacy", thirdLabel: "Block Accounts")
            ,settingStruct(imageVar: #imageLiteral(resourceName: "password"), firstLabel: "Passwords", secondLabel: "Login Activity", thirdLabel: "Saved Login lnk")
            ,settingStruct(imageVar: #imageLiteral(resourceName: "ads"), firstLabel: "", secondLabel: "About Ads", thirdLabel: "")
            ,settingStruct(imageVar: #imageLiteral(resourceName: "report_a_problem"), firstLabel: "Report a problem", secondLabel: "Help Center", thirdLabel: "Privacy & Security")
            ,settingStruct(imageVar: #imageLiteral(resourceName: "datapolicy"), firstLabel: "Data Policy", secondLabel: "Terms of Use", thirdLabel: "Open Source Libraries")
            ]
        /*,settingStruct(imageVar: #imageLiteral(resourceName: "comments,account"), firstLabel: "Comments", secondLabel: "Account Privacy", thirdLabel: "Block Accounts")
        ,settingStruct(imageVar: #imageLiteral(resourceName: "password"), firstLabel: "Passwords", secondLabel: "Login Activity", thirdLabel: "Saved Login lnk")
        ,settingStruct(imageVar: #imageLiteral(resourceName: "ads"), firstLabel: "", secondLabel: "About Ads", thirdLabel: "")
        ,settingStruct(imageVar: #imageLiteral(resourceName: "report_a_problem"), firstLabel: "Report a problem", secondLabel: "Help Center", thirdLabel: "Privacy & Security")
        ,settingStruct(imageVar: #imageLiteral(resourceName: "datapolicy"), firstLabel: "Data Policy", secondLabel: "Terms of Use", thirdLabel: "Open Source Libraries")*/
        setNavLeftImage(image: #imageLiteral(resourceName: "small logo dummy"))
        msgTableView.reloadData()
    }
    //MARK:- delegates
    
}
extension SettingVC:UITableViewDelegate,UITableViewDataSource{
    //MARK:- TableView delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return settingArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return AppDelegate.deviceRatio/3
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let SettingData = settingArray[indexPath.row]
        print(settingArray.count)
        guard let cell = msgTableView.dequeueReusableCell(withIdentifier: "SettingCell", for: indexPath) as? SettingCell else {
            return UITableViewCell()
        }
         cell.msgImg.image = SettingData.imageVar
        cell.fistLbl.text = SettingData.firstLabel
        cell.secondLbl.text = SettingData.secondLabel
        cell.msgLbl.text = SettingData.thirdLabel
        return cell
    }
    
}
struct settingStruct{
    var imageVar : UIImage?
    var firstLabel : String?
    var secondLabel : String?
    var thirdLabel : String?
}
