//
//  LoginVC.swift
//  Global Farm Market
//
//  Created by M.Hashim on 01/11/2019.
//  Copyright © 2019 index. All rights reserved.
//

import UIKit

class LoginVC: BaseVC {
    //MARK:- OUTLETS
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!
    @IBOutlet weak var forgotPasswordLbl: UILabel!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var faceBookBtn: UIButton!
    @IBOutlet weak var googleBtn: UIButton!
    @IBOutlet weak var signUpLbl: UILabel!
    
    //MARK:- VARIABLES
    
    //MARK:- LIFECYLCE
    override func viewDidLoad() {
        super.viewDidLoad()
        setLayOut()
        gotoSignUp()
        emailTxt.text = "g@gmail.com"
        passwordTxt.text = "qweqwe123"
    }
    override func viewWillAppear(_ animated: Bool) {
        //refreshFields()
    }
    @objc func tapFunction(sender:UITapGestureRecognizer) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SignUpVC") as? SignUpVC
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func loginAction(_ sender: Any) {
        if validateEmailPassword(emailText: emailTxt.text ?? "", password: passwordTxt.text ?? "") != false{
            gotoDashboard()
        }
        else{
            makeAlert(messageData: "Field are Invaild")
        }
    }
    func gotoSignUp(){
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapFunction(sender:)))
        signUpLbl.isUserInteractionEnabled = true
        signUpLbl.addGestureRecognizer(tap)
    }
    func setLayOut(){
        emailTxt.layer.cornerRadius = emailTxt.frame.height/2
        passwordTxt.layer.cornerRadius = passwordTxt.frame.height/2
        loginBtn.layer.cornerRadius = loginBtn.frame.height/2
        faceBookBtn.layer.cornerRadius = faceBookBtn.frame.height/2
        googleBtn.layer.cornerRadius = googleBtn.frame.height/2
        self.navigationController?.navigationBar.isHidden=true
        
    }
    func refreshFields(tag:Int?=0){
        emailTxt.text = ""
        passwordTxt.text = ""
        emailTxt.becomeFirstResponder()
    }
    //MARK:-GotoTab
    func gotoDashboard(){
        let tabBarVC = UIStoryboard(name: "dashBoard", bundle: nil).instantiateViewController(withIdentifier: "tabBarVC") as! tabBarVC
        tabBarVC.selectedIndex = 1 //your selected tab index
        navigationController?.pushViewController(tabBarVC, animated: true)
        
    }
}


