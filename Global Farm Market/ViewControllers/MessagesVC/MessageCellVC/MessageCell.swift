//
//  MessageCell.swift
//  Global Farm Market
//
//  Created by M.Hashim on 04/11/2019.
//  Copyright © 2019 index. All rights reserved.
//

import UIKit

class MessageCell: UITableViewCell {
    //MARK:- Outlets
    @IBOutlet weak var msgImage: UIImageView!
    @IBOutlet weak var msgUserName: UILabel!
    @IBOutlet weak var farmValue: UILabel!
    @IBOutlet weak var msgLbl: UILabel!
    @IBOutlet weak var msgTime: UILabel!
    //MARK:- LIFECYLCE
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        circleShap(imageMsg: msgImage)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    func circleShap(imageMsg:UIImageView){
        imageMsg.layer.cornerRadius = imageMsg.frame.height/2
    }
}
