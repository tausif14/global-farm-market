//
//  MessageVC.swift
//  Global Farm Market
//
//  Created by M.Hashim on 04/11/2019.
//  Copyright © 2019 index. All rights reserved.
//

import UIKit

class MessageVC: BaseVC {

    //MARK:- Outlets
    @IBOutlet weak var msgTableView: UITableView!
    //MARK:- LIFECYLCE
    override func viewDidLoad() {
        super.viewDidLoad()
        
       setNavLeftImage(image: #imageLiteral(resourceName: "small logo dummy"))
    }
  //MARK:- delegates

}
extension MessageVC:UITableViewDelegate,UITableViewDataSource{
    //MARK:- TableView delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 22
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = msgTableView.dequeueReusableCell(withIdentifier: "MessageCell", for: indexPath) as? MessageCell else {
            return UITableViewCell()
            
            
        }
        return cell
    }
    
}
