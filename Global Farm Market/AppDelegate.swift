//
//  AppDelegate.swift
//  Global Farm Market
//
//  Created by M.Hashim on 01/11/2019.
//  Copyright © 2019 index. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
   
    static var deviceRatio : CGFloat = 0.0


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
//        window = UIWindow(frame: UIScreen.main.bounds)
//        let storyboard = UIStoryboard.init(name: "dashBoard", bundle: nil)
//        let viewController = storyboard.instantiateViewController(withIdentifier: "tabBarVC") as! tabBarVC
//        viewController.selectedIndex = 1
//        self.window?.rootViewController = viewController
//        self.window?.makeKeyAndVisible()
        AppDelegate.deviceRatio = UIScreen.main.bounds.width/2
        print("**********Device Size is : \(AppDelegate.deviceRatio)*********")
         return true
           
 }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

