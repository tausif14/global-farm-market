//
//  BaseVC.swift
//  Global Farm Market
//
//  Created by M.Hashim on 04/11/2019.
//  Copyright © 2019 index. All rights reserved.
//

import UIKit

class BaseVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
   func setNavfontSize(){
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont(name:"System", size: 25.0)!]

    }
    //MARK:-AlertMsg
    func makeAlert(titleMsg:String?="Error",messageData:String){
        let alert = UIAlertController(title: titleMsg, message: messageData, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    //MARK:-Validation Fields
    func validateEmailPassword(emailText:String,password:String,confrimPassward:String? = "")->Bool{
        if !(emailText.isEmpty) || !(password.isEmpty){
            if emailText.isValidEmail && password.isValidPassword{
                // makeAlert(titleMsg: "Congrats", messageData: "Successfully Login")
                return true
            }
            else if !emailText.isValidEmail{
                makeAlert(messageData: "InVaild Email")
                return true
            }
            else if !password.isValidPassword{
                makeAlert(messageData: "Password Should contain Minimum 8 characters at least 1 Alphabet and 1 Number")
                return true
            }
            else if !emailText.isValidEmail && !password.isValidPassword {
                makeAlert(messageData: "Enter Fields are not Vaild")
                return true
            }
            else if checkPassword(password: password , confrimPassword: confrimPassward ?? ""){
                print("inside Password Check ")
                return true
            }
        }
        
        return false
    }
    //MARK:-BackAction
    func goBack(){
        self.navigationController?.popViewController(animated: true)
    }
    //MARK:-ChangePassword
    func checkPassword(password:String,confrimPassword:String)->Bool{
        if confrimPassword == password{
            print("Password Matches")
            return true
        }
        else if confrimPassword != password {
            makeAlert(messageData:"Pasword Doesn't Match")
            return true
        }
        return false
    }
    //MARK:SetNavigationLeftImage
    func setNavLeftImage(image:UIImage){
        let containView = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        let imageview = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        imageview.image = image
        imageview.contentMode = UIView.ContentMode.scaleAspectFit
        containView.addSubview(imageview)
        let leftBarButton = UIBarButtonItem(customView: containView)
        self.navigationItem.leftBarButtonItem = leftBarButton
    }
    //MARK:SetNavigationRightImage
    func setNavRightImage(image:UIImage){
        let containView = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        let imageview = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        imageview.image = image
        imageview.contentMode = UIView.ContentMode.scaleAspectFit
        containView.addSubview(imageview)
        let rightBarButton = UIBarButtonItem(customView: containView)
        self.navigationItem.rightBarButtonItem = rightBarButton
    }
    //MARK:SetCornerSmallRadius
    func circleShap(imageMsg:UIImageView?=nil,actionBtn:UIButton?=nil,customView:UIView?=nil){
         imageMsg?.layer.cornerRadius = AppDelegate.deviceRatio/12
         actionBtn?.layer.cornerRadius = AppDelegate.deviceRatio/12
        customView?.layer.cornerRadius = AppDelegate.deviceRatio/12
         print("*********DeviceRatio Value for Cell :\(AppDelegate.deviceRatio/12)*********")
    }
}
//MARK:REGEX
extension String {
    var isValidEmail: Bool {
        return NSPredicate(format: "SELF MATCHES %@", "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}").evaluate(with: self)
    }
    var isValidPassword:Bool{
        return NSPredicate(format: "SELF MATCHES %@", "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$").evaluate(with: self)
    }
    
}
extension UITextField{
    //MARK:-RefreshFields
    func setTextClear(txt:UITextField)->String?{
        txt.text = ""
        let getClear = txt.text
        return getClear
    }
}
