//
//  tabBarVC.swift
//  Global Farm Market
//
//  Created by M.Hashim on 04/11/2019.
//  Copyright © 2019 index. All rights reserved.
//

import UIKit

class tabBarVC: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        goto()
        // Do any additional setup after loading the view.
    }
    
    func goto(){
        let accVC = AccountVC()
        let cartVC = CartVC()
        let msgVC = MessageVC()
        let locationVC = LocationVC()
        let settingVC = SettingVC()
        
        let tabBarControl = UITabBarController()
        tabBarControl.viewControllers = [accVC, cartVC, msgVC,locationVC,settingVC]
        //tabBarControl.selectedIndex = 1
        
    }
}
